#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h> // for initializing the random number generator

#include "raylib.h"

#define EMULATION_SPEED 1/750.0f 
#define UPDATE_SPEED 1/60.0f
static Sound beep;

typedef enum EmulatorState
{
    STATE_START = 0,
    STATE_GAMEPLAY,
} EmulatorState;

static uint8_t fontset[80] = 
{
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

static KeyboardKey keymap[16] =
{
    KEY_X,
    KEY_ONE,
    KEY_TWO,
    KEY_THREE,
    KEY_Q,
    KEY_W,
    KEY_A,
    KEY_S,
    KEY_D,
    KEY_Z,
    KEY_C,
    KEY_FOUR,
    KEY_R,
    KEY_F,
    KEY_V
};

typedef struct State
{
    uint8_t memory[4096];
    uint8_t display[32][64]; // 64x32 (128x64 for SUPER-CHIP)
    uint16_t PC;
    uint16_t I;
    uint16_t stack[16];
    uint16_t stack_pointer;
    uint8_t delay_timer;
    uint8_t sound_timer;
    float emulation_speed_accumulator;
    float update_speed_accumulator;
    uint8_t registers[16];
    uint8_t keyboard[16];
    int16_t waiting_key;
    bool display_changed;
    EmulatorState emulator_state;
} State;

static State state = {0};

void emulator_init()
{
    // initialize random number genrator
    time_t t;
    srand((unsigned) time(&t));

    // reset emulator
    memset(&state, 0, sizeof(State));
    state.PC = 0x200;
    state.display_changed = false;
    state.waiting_key = -1;
    // load fontset
    memcpy(&state.memory, fontset, sizeof(uint8_t)*80);
}

void load_rom()
{
    FilePathList dropped_files = LoadDroppedFiles();

    // Check file extensions for drag-and-drop
    if ((dropped_files.count == 1) && IsFileExtension(dropped_files.paths[0], ".ch8"))
    {
        fprintf(stdout, "Dropped file. \n");

        FILE *fp = fopen(dropped_files.paths[0], "rb");
        
        if (fp == NULL)
        {
            fprintf(stderr, "Can't open the file rom \n");
        }

        fseek(fp, 0, SEEK_END);
        int data_size = ftell(fp);
        fseek(fp, 0, SEEK_SET);

        fread(state.memory + 0x200, sizeof(uint16_t), data_size, fp);

        fclose(fp);
        state.emulator_state = STATE_GAMEPLAY;
    }

    UnloadDroppedFiles(dropped_files);
}

void emulator_input()
{
    // if (IsKeyPressed(KEY_ESCAPE))
    // {
    //     emulator_init();
    // }

    if (IsKeyDown(KEY_ONE)) state.keyboard[0x1] = 1;
    else if (IsKeyUp(KEY_ONE)) state.keyboard[0x1] = 0;

    if (IsKeyDown(KEY_TWO)) state.keyboard[0x2] = 1;
    else if (IsKeyUp(KEY_TWO)) state.keyboard[0x2] = 0;

    if (IsKeyDown(KEY_THREE)) state.keyboard[0x3] = 1;
    else if (IsKeyUp(KEY_THREE)) state.keyboard[0x3] = 0;

    if (IsKeyDown(KEY_FOUR)) state.keyboard[0xC] = 1;
    else if (IsKeyUp(KEY_FOUR)) state.keyboard[0xC] = 0;

    if (IsKeyDown(KEY_Q)) state.keyboard[0x4] = 1;
    else if (IsKeyUp(KEY_Q)) state.keyboard[0x4] = 0;

    if (IsKeyDown(KEY_W)) state.keyboard[0x5] = 1;
    else if (IsKeyUp(KEY_W)) state.keyboard[0x5] = 0;

    if (IsKeyDown(KEY_E)) state.keyboard[0x6] = 1;
    else if (IsKeyUp(KEY_E)) state.keyboard[0x6] = 0;

    if (IsKeyDown(KEY_R)) state.keyboard[0xD] = 1;
    else if (IsKeyUp(KEY_R)) state.keyboard[0xD] = 0;

    if (IsKeyDown(KEY_A)) state.keyboard[0x7] = 1;
    else if (IsKeyUp(KEY_A)) state.keyboard[0x7] = 0;

    if (IsKeyDown(KEY_S)) state.keyboard[0x8] = 1;
    else if (IsKeyUp(KEY_S)) state.keyboard[0x8] = 0;

    if (IsKeyDown(KEY_D)) state.keyboard[0x9] = 1;
    else if (IsKeyUp(KEY_D)) state.keyboard[0x9] = 0;

    if (IsKeyDown(KEY_F)) state.keyboard[0xE] = 1;
    else if (IsKeyUp(KEY_F)) state.keyboard[0xE] = 0;

    if (IsKeyDown(KEY_Z)) state.keyboard[0xA] = 1;
    else if (IsKeyUp(KEY_Z)) state.keyboard[0xA] = 0;

    if (IsKeyDown(KEY_X)) state.keyboard[0x0] = 1;
    else if (IsKeyUp(KEY_X)) state.keyboard[0x0] = 0;

    if (IsKeyDown(KEY_C)) state.keyboard[0xB] = 1;
    else if (IsKeyUp(KEY_C)) state.keyboard[0xB] = 0;

    if (IsKeyDown(KEY_V)) state.keyboard[0xF] = 1;
    else if (IsKeyUp(KEY_V)) state.keyboard[0xF] = 0;
}

void timer_update()
{
    if (state.delay_timer > 0)
    {
        state.delay_timer--;
    }

    if (state.sound_timer > 0)
    {
        state.sound_timer--;
        if (!IsSoundPlaying(beep))
        {
            PlaySound(beep);
        }
    }
}

void emulator_update()
{
    // input_update();
    uint16_t opcode = (state.memory[state.PC] << 8) | state.memory[state.PC+1];
    uint8_t X = (opcode & 0x0F00) >> 8;
    uint8_t Y = (opcode & 0x00F0) >> 4;
    state.PC += 2;

    switch (opcode & 0xF000)
    {
        case 0x0000:
        {
            switch (opcode & 0x00FF)
            {
                // 0x00E0 Clear screen
                case 0x00E0:
                {
                    memset(state.display, 0, sizeof(state.display));
                    state.display_changed = true;
                } break;

                // 0x00EE Return from subroutine
                case 0x00EE:
                {
                    state.stack_pointer--;
                    state.PC = state.stack[state.stack_pointer];
                } break;
            }
        } break;

        // 0x1NNN Jump
        case 0x1000:
        {
            state.PC = opcode & 0x0FFF;
        } break;

        // 0x2NNN Call subroutine
        case 0x2000:
        {
            state.stack[state.stack_pointer] = state.PC;
            state.stack_pointer++;
            state.PC = opcode & 0x0FFF;
        } break;

        // 0x3XNN Skip one instruction if VX is equal to NN
        case 0x3000:
        {
            if (state.registers[X] == (opcode & 0x00FF))
            {
                state.PC += 2;
            }
        } break;

        // 0x4XNN Skip one instruction if VX is NOT equal to NN
        case 0x4000:
        {
            if (state.registers[X] != (opcode & 0x00FF))
            {
                state.PC += 2;
            }
        } break;

        // 0x5XY0 Skip one instruction if VX is equal to VY
        case 0x5000:
        {
            if (state.registers[X] == state.registers[Y])
            {
                state.PC += 2;
            }
        } break;

        // 0x6XNN Set
        case 0x6000:
        {
            state.registers[X] = opcode & 0x00FF;
        } break;

        // 0x7XNN Add
        case 0x7000:
        {
            state.registers[X] += (opcode & 0x00FF);
        } break;

        case 0x8000:
        {
            switch (opcode & 0x000F)
            {
                // 0x8XY0 Set
                case 0x0000:
                {
                    state.registers[X] = state.registers[Y];
                } break;

                // 0x8XY1 Binary OR
                case 0x0001:
                {
                    state.registers[X] |= state.registers[Y];
                    state.registers[0xF] = 0; // CHIP-8 specific quirk
                } break;

                // 0x8XY2 Binary AND
                case 0x0002:
                {
                    state.registers[X] &= state.registers[Y];
                    state.registers[0xF] = 0; // CHIP-8 specific quirk
                } break;

                // 0x8XY3 Logical XOR
                case 0x0003:
                {
                    state.registers[X] ^= state.registers[Y];
                    state.registers[0xF] = 0; // CHIP-8 specific quirk
                } break;

                // 0x8XY4 ADD
                case 0x0004:
                {
                    uint8_t carry = (255 - state.registers[X]) < state.registers[Y] ? 1 : 0;
                    state.registers[X] += state.registers[Y];
                    state.registers[0xF] = carry;
                } break;

                // 0x8XY5 Subtract
                case 0x0005:
                {
                    uint8_t carry = state.registers[X] >= state.registers[Y] ? 1 : 0;
                    state.registers[X] -= state.registers[Y];
                    state.registers[0xF] = carry;
                } break;

                // 0x8XY6 Shift right (Ambiguous instruction!)
                case 0x0006:
                {
                    state.registers[X] = state.registers[Y]; // optional or configurable
                    uint8_t carry = (state.registers[X] & 0x0001);
                    state.registers[X] = state.registers[X] >> 1;
                    state.registers[0xF] = carry;
                } break;

                // 0x8XYE Shift left (Ambiguous instruction!)
                case 0x000E:
                {
                    state.registers[X] = state.registers[Y]; // optional or configurable
                    uint8_t carry = (state.registers[X] >> 7);
                    state.registers[X] = state.registers[X] << 1;
                    state.registers[0xF] = carry;
                } break;

                // 0x8XY7 Subtract
                case 0x0007:
                {
                    uint8_t carry = state.registers[Y] >= state.registers[X] ? 1 : 0;
                    state.registers[X] = state.registers[Y] - state.registers[X];
                    state.registers[0xF] = carry;
                } break;
            }
        } break;

        // 0x9XY0 Skip one instruction if VX is NOT equal to VY
        case 0x9000:
        {
            if (state.registers[X] != state.registers[Y])
            {
                state.PC += 2;
            }
        } break;

        // 0xANNN Set index
        case 0xA000:
        {
            state.I = opcode & 0x0FFF;
        } break;

        // 0xBNNN Jump with offset (Ambiguous instruction!)
        case 0xB000:
        {
            state.PC = (opcode & 0x0FFF) + (state.registers[0x0]);
        } break;

        // 0xCXNN Random
        case 0xC000:
        {
            state.registers[X] = rand() & (opcode & 0x00FF);
        } break;

        // 0xDXYN Display
        case 0xD000:
        {
            uint8_t x_pos = state.registers[X] % 64;
            uint8_t y_pos = state.registers[Y] % 32;
            state.registers[0xF] = 0; // Set VF to 0
            uint8_t sprite_data;
            for (uint8_t n = 0; n < (opcode & 0x000F); n++)
            {
                sprite_data = state.memory[state.I + n];

                uint8_t sprite_y = y_pos + n;
                if (sprite_y >= 32)
                {
                    break;
                }

                for (uint8_t i = 0; i < 8; i++)
                {
                    uint8_t sprite_x = x_pos + (7 - i);
                    if (sprite_x < 0 || sprite_x >= 64)
                    {
                        continue;
                    }

                    if ((sprite_data & (0x1 << i)))
                    {
                        if (state.display[sprite_y][sprite_x])
                        {
                            state.display[sprite_y][sprite_x] = 0;
                            state.registers[0xF] = 1; // Set VF to 1
                        }
                        else
                        {
                            state.display[sprite_y][sprite_x] = 1;
                        }
                    }
                }
            }
            state.display_changed = true;
        } break;

        case 0xE000:
        {
            switch (opcode & 0x00FF)
            {
                // 0xEX9E Skip if key pressed
                case 0x009E:
                {
                    if (state.keyboard[state.registers[X]] == 1)
                    {
                        state.PC += 2;
                    }
                } break;

                // 0xEXA1 Skip if key NOT pressed
                case 0x00A1:
                {
                    if (state.keyboard[state.registers[X]] == 0)
                    {
                        state.PC += 2;
                    }
                } break;
            }
        } break;

        case 0xF000:
        {
            switch (opcode & 0x00FF)
            {
                // 0xFX07 Timer
                case 0x0007:
                {
                    state.registers[X] = state.delay_timer;
                } break;

                // 0xFX15 Timer
                case 0x0015:
                {
                    state.delay_timer = state.registers[X];
                } break;

                // 0xFX18 Timer
                case 0x0018:
                {
                    state.sound_timer = state.registers[X];
                } break;

                // 0xFX1E Add to index
                case 0x001E:
                {
                    state.I += state.registers[X];
                    if (state.I > 1000)
                    {
                        state.registers[0xF] = 1;
                    }
                } break;

                // 0xFX0A Get key
                case 0x000A:
                {
                    state.waiting_key = X;
                    if (state.waiting_key != -1)
                    {
                        for (uint8_t i = 0; i < 16; i++)
                        {
                            if (IsKeyReleased(keymap[i]))
                            {
                                state.registers[(uint16_t)state.waiting_key] = i;
                                state.waiting_key = -1;
                                break;
                            }
                        }
                        if (state.waiting_key != -1)
                        {
                            state.PC -= 2;
                        }
                    }
                } break;

                // 0xFX29 Font character
                case 0x0029:
                {
                    state.I = state.registers[X] * 5;
                } break;

                // 0xFX33 Binary-coded decimal conversion
                case 0x0033:
                {
                    uint8_t vx = state.registers[X];
                    state.memory[state.I] = (vx - (vx % 100)) / 100;
                    vx -= state.memory[state.I] * 100;
                    state.memory[state.I + 1] = (vx - (vx % 10)) / 10;
                    vx -= state.memory[state.I + 1] * 10;
                    state.memory[state.I + 2] = vx;
                } break;

                // 0xFX55 Store memory
                case 0x0055:
                {
                    for (uint8_t i = 0; i <= X; i++)
                    {
                        state.memory[state.I + i] = state.registers[i];
                    }
                    state.I++; // CHIP-8 specific quirk
                } break;

                // 0xFX65 Load memory
                case 0x0065:
                {
                    for (uint8_t i = 0; i <= X; i++)
                    {
                        state.registers[i] = state.memory[state.I + i];
                    }
                    state.I++; // CHIP-8 specific quirk
                } break;
            }
        } break;

        default: { printf("ERROR UNKNOWN OPCODE: %x \n",opcode); } break;
    }
}

void emulator_render()
{
    BeginDrawing();
        ClearBackground((Color){1, 17, 0, 255});

        for (int y = 0; y < 32; y++)
        {
            for (int x = 0; x < 64; x++)
            {
                if (state.display[y][x])
                {
                    DrawRectangle(x*10, y*10, 10, 10, (Color){225, 255, 254, 255});
                }
            }
        }
        state.display_changed = false;
    EndDrawing();
}

int main()
{
    // SetConfigFlags(FLAG_BORDERLESS_WINDOWED_MODE);
    InitWindow(640, 320,"CHIP-8 Emulator");
    SetTargetFPS(60);
    // SetExitKey(0); // prevents ESCAPE from closing the whole program

    InitAudioDevice();
	SetMasterVolume(0.25);
    beep = LoadSound("../data/beep.wav");

    emulator_init();

    while (!WindowShouldClose())
    {
        switch (state.emulator_state)
        {
            case STATE_START:
            {
                load_rom();
                BeginDrawing();
                    ClearBackground((Color){1, 17, 0, 255});
                    DrawText(
                        "Drag and Drop a ROM to run.", 
                        (640 - MeasureText("Drag and Drop a ROM to run.", 30))/2, 
                        320/2, 
                        30, 
                        (Color){225, 255, 254, 255}
                    );
                EndDrawing();
            } break;
            case STATE_GAMEPLAY:
            {

                float delta_time = GetFrameTime();

                emulator_input();

                state.emulation_speed_accumulator += delta_time;
                state.update_speed_accumulator += delta_time;

                while (state.emulation_speed_accumulator >= EMULATION_SPEED)
                {
                    emulator_update();
                    state.emulation_speed_accumulator -= EMULATION_SPEED;
                }

                while (state.update_speed_accumulator >= UPDATE_SPEED)
                {
                    timer_update();
                    state.update_speed_accumulator -= UPDATE_SPEED;
                }
                emulator_render();
            } break;
            default: {} break;
        }
    }

    UnloadSound(beep);
    CloseAudioDevice();
    CloseWindow();

    return 0;
}