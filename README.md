# chip-8_rl

A simple CHIP-8 emulator written in C.

The ```roms``` folder includes some game roms to run with the emulator. Just drag them into the running emulator to load them.

All roms are from https://www.zophar.net/pdroms/chip8/chip-8-games-pack.html and https://johnearnest.github.io/chip8Archive/.

Do note that not all games work on the emulator, because of implementation quirks of different CHIP-8 systems.

The ```beep.wav``` has been created with https://raylibtech.itch.io/rfxgen.

## Controls

The CHIP-8 keypad

|   |   |   |   |
|---|---|---|---|
| 1 | 2 | 3 | C |
| 4 | 5 | 6 | D |
| 7 | 8 | 9 | E |
| A | 0 | B | F |

is mapped to 

|   |   |   |   |
|---|---|---|---|
| 1 | 2 | 3 | 4 |
| Q | W | E | R |
| A | S | D | F |
| Z | X | C | V |

## Build

This has only been tested on Windows. You need ```gcc``` to build it. It should be in your PATH.

Run either ```build.bat``` or ```build.bat release``` to compile a debug or release build respectively.

## Screenshots

![emulator_start](screenshots/emulator_start.png)

![emulator_space_invaders](screenshots/emulator_space_invaders.png)

![emulator_brix](screenshots/emulator_brix.png)

## Resources

https://tobiasvl.github.io/blog/write-a-chip-8-emulator/

https://github.com/Timendus/chip8-test-suite?tab=readme-ov-file