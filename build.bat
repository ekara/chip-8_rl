@echo off
setlocal
cd /D "%~dp0"

:: --- Unpack Arguments -------------------------------------------------------
for %%a in (%*) do set "%%a=1"
if not "%mingw%"=="1"   set mingw=1
if not "%release%"=="1" set debug=1
if "%debug%"=="1"       set release=0 && echo [debug mode]
if "%release%"=="1"     set debug=0 && echo [release mode]
if "%mingw%"=="1"       echo [mingw compile]

:: --- Compile/Link Line Definitions ------------------------------------------
set mingw_common=  -I..\external\raylib\include -Wall -Wextra -Wpedantic -std=c99 -Wno-missing-braces
set mingw_debug=   call gcc %mingw_common% -O0 -g -D_DEBUG
set mingw_release= call gcc %mingw_common% -O1 -Wl,--subsystem,windows
set mingw_lib=     -L..\external\raylib\lib\mingw -lraylib -lopengl32 -lgdi32 -lwinmm -lcomdlg32 -lole32 -static -lpthread
set mingw_link=    %mingw_lib%
set mingw_out=     -o

:: --- Choose Compile/Link Lines ----------------------------------------------
if "%mingw%"=="1"   set compile_debug=%mingw_debug%
if "%mingw%"=="1"   set compile_release=%mingw_release%
if "%mingw%"=="1"   set compile_link=%mingw_link%
if "%mingw%"=="1"   set out=%mingw_out%
if "%debug%"=="1"   set compile=%compile_debug%
if "%release%"=="1" set compile=%compile_release%

:: --- Prep Directories -------------------------------------------------------
if not exist build mkdir build

:: --- Build Everything -------------------------------------------------------
pushd build
del . /F /Q
%compile% ..\code\main.c %compile_link% %out%game.exe || exit /b 1
popd